public class Mass {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.print("Enter massiv elements: ");
		double arr[] = new double[10];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = input.nextDouble();
		}
		input.close();

		System.out.println("Massiv: ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}

		System.out.println();

		for (int i = 0; i < arr.length; i++) {
			arr[i] = (arr[i]) * 0.1 + arr[i];
		}

		System.out.println("Massiv after increasing each element by 10%: ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}

		for (int start = 0; start < arr.length - 1; start++) {
			for (int index = 0; index < arr.length - start - 1; index++) {
				if (arr[index] < arr[index + 1]) {
					swap(arr, index);
				}
			}
		}
		System.out.print("\n");
		System.out.print("Massiv elements after bubble sorting in descending order: ");
		System.out.print("\n");
		System.out.println(Arrays.toString(arr));
	}

	public static void swap(double arr[], int i) {
		double tmp = arr[i];
		arr[i] = arr[(i + 1)];
		arr[(i + 1)] = tmp;
	}
}